﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserColor : MonoBehaviour {

    float transparency = 0.6f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider coll)
    {
        GameObject collided = coll.gameObject;
        //Debug.Log("Collision with object " + collided.name);
        if (collided.CompareTag("PlayerBody"))
        {

            GetComponent<Renderer>().material.color = Color.yellow + new Color(0, 0, 0, -1 + transparency);
        }
    }

	void OnTrigger(Collider coll)
	{
		GameObject collided = coll.gameObject;
		Debug.Log("Collision with object " + collided.name);
		if (collided.CompareTag("PlayerBody"))
		{

			GetComponent<Renderer>().material.color = Color.yellow + new Color(0, 0, 0, -1 + transparency);
		}
	}
    
    void OnTriggerExit(Collider coll)
    {
        GameObject collided = coll.gameObject;
        if (collided.CompareTag("PlayerBody"))
        {
            GetComponent<Renderer>().material.color = Color.blue + new Color(0, 0, 0, -1 + transparency);
        }
    }
}
