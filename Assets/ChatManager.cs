﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChatManager : MonoBehaviour
{
    public int limit;
  
    public List<string> mArray = new List<string>();
    public List<Vector3> pArray = new List<Vector3>();


    void Start()
    {
        limit = 5; 
    }
    
    public void setValues(string str, Vector3 pos1)
    {
        if (mArray.Count >= limit)
        {
            mArray.RemoveAt(0);
            pArray.RemoveAt(0);

        }

        mArray.Add(str);
        pArray.Add(pos1);
    }

   

}