﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;

public class PlayerManagerScript : NetworkBehaviour {

    [SyncVar]
    public bool player3_can_act = true;

    private float delay_attempt;
    private float delay_player3;
    private const float DELAY_ATTEMPT_TIME = 2.0f;
    private const float DELAY_PLAYER3_TIME = 8.0f;

    private int count;

	// Use this for initialization
	void Start () {
        count = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if (delay_attempt > 0)
            delay_attempt -= Time.deltaTime;
        if (delay_player3 > 0)
            delay_player3 -= Time.deltaTime;
        if (delay_attempt <= 0)
            count = 0;
        if (delay_player3 <= 0 && !player3_can_act)
            player3_can_act = true;

    }

    public void ReceiveParalysisAttempt()
    {
        if(count == 0)
        {
            delay_attempt = DELAY_ATTEMPT_TIME;
        }
        count++;

        if (count == 2)
        {
            delay_player3 = DELAY_PLAYER3_TIME;
            player3_can_act = false;
            count = 0;
        }
    }

    public bool CanPlayer3Act()
	{
        return player3_can_act;
	}

	public void StopPlayer3()
	{
		player3_can_act = false;
		delay_player3 = 999999.0f;
	}
}
