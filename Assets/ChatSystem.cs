﻿using System.Collections;
using UnityEngine.Networking;
using UnityEngine;

public class ChatSystem : NetworkBehaviour
{

    public string message;
    public Vector3 position;
    
    public ChatManager Manager;

    void Start()
    {
        Manager = GameObject.Find("NetworkManager").GetComponent<ChatManager>();
    }

    [Command]
    void CmdSendMessageToServer(string words, Vector3 pos)
    {
        RpcSentMessageToClients(words, pos);
    }

    [ClientRpc]
    void RpcSentMessageToClients(string words, Vector3 pos)
    {
        Manager.setValues(words, pos);
    }

    public void sendValues(string str, Vector3 pos)
    {
        message = str;
        position = pos;
    }

    //if this person is a local player show the inpt to write a message
    void OnGUI()
    {
        if (isLocalPlayer)
        {
            if (message != null)
            {
                if (transform.GetComponent<NetworkIdentity>().isServer)
                {
                    RpcSentMessageToClients(message, position);
                }
                else
                {
                    CmdSendMessageToServer(message, position);
                }
                message = null;
            }
        }
    }
}