﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour {

    private float lifeCounter, MAXLIFE = 3f;


	// Use this for initialization
	void Start () {
        lifeCounter = 0;
	}
	
	// Update is called once per frame
	void Update () {
        lifeCounter += Time.deltaTime;
        if (lifeCounter > MAXLIFE) {
            Destroy(gameObject);
            
        }
	}

    
}
