﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Bullet : NetworkBehaviour {

    public int damage;
	public bool deviated;


	void Update(){

		Rigidbody rigidObj = GetComponent<Rigidbody> ();
		rigidObj.angularVelocity = new Vector3 (0, 0, 0);

		Transform transfObj = GetComponent<Transform> ();
		//transfObj.LookAt(transfObj.position + transfObj.forward);

		rigidObj.AddForce(rigidObj.
/*
		RaycastHit hit;

	
		if (Physics.SphereCast(
		{
			distanceToObstacle = hit.distance;
		}
*/
	}

    void OnCollisionEnter(Collision collision)
    {
        var hit = collision.gameObject;
        var health = hit.GetComponent<Health>();
        if (health != null)
            health.TakeDamage(damage);
		if (hit.CompareTag("GameController")) {
			Transform transfObj = GetComponent<Transform> ();
			Rigidbody rigidObj = GetComponent<Rigidbody> ();
			Vector3 posi = transfObj.position;
			Vector3 contact = collision.contacts[0].point;
			Vector3 direction = posi - contact;
			transfObj.LookAt(posi + direction);
			rigidObj.angularVelocity = new Vector3 (0, 0, 0);
			rigidObj.velocity = 0;
		}
		else
        	Destroy(gameObject);


    }


}
