﻿using UnityEngine;
using UnityEngine.Networking;
using System;

public class NetCorrector : NetworkBehaviour
{
    [SyncVar]
    public Vector3 scale;
    [SyncVar]
    public Quaternion rotation;

    void Start()
    {
        transform.localScale = scale;
        transform.rotation = rotation;
    
    }

    public void UpdateSize()
    {
        transform.localScale = scale;
        transform.rotation = rotation;
    }
}