﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class NetworkManagerAsymetric : NetworkManager {

    void Start()
    {
        Debug.Log("START REACHED");
    }

    public GameObject player3Prefab;
    public Transform player3Spawn;
    private int playerCounter;

    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
    {
        Debug.Log("Function Reached");
        playerCounter++;
        if (playerCounter == 1) {
            Debug.Log("3rd player Reached");
            var player = (GameObject)GameObject.Instantiate(player3Prefab, player3Spawn, true);
            player.transform.localPosition = Vector3.zero;
            NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
        }
        else
        {
            Transform posi = GetStartPosition();
            var player = (GameObject)GameObject.Instantiate(playerPrefab, posi, true);
            player.transform.localPosition = Vector3.zero;
            NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
        }

        Debug.Log(playerCounter);
    }

}
