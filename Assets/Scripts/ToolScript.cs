﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ToolScript : NetworkBehaviour {

    public const int NO_TOOL_SELECTED = 0;
    public const int GUN_TOOL_SELECTED = 1;
    public const int WALL_TOOL_SELECTED = 2;
    public const int KILL_GUN_TOOL_SELECTED = 3;
	public const int HEAL_GUN_TOOL_SELECTED = 4;
    public const int COOP_TOOL_SELECTED = 5;

    public const int AWAITING_RELOAD = 6;

	public const float GUN_TOOL_RELOAD_TIME = 0.1f;
    public const float KILL_GUN_TOOL_RELOAD_TIME = 0.1f;
	public const float HEAL_GUN_TOOL_RELOAD_TIME = 0.1f;
	public const float WALL_RELOAD_TIME = 0.1f;
	public const float COOP_RELOAD_TIME = 0.1f;

    public const float PROJECTILE_SPEED = 2.0f;

    public GameObject bullet_prefab;
    public GameObject kill_bullet_prefab;
    public GameObject wall_prefab;
    public GameObject heal_bullet_prefab;

    public GameObject origin;
	private Transform origin_pos;
	public GameObject destination;

    public Camera player_camera;

    public int toolState = GUN_TOOL_SELECTED;

	public float delay;
	public bool isWaiting;
	public float current_tool_delay;
	public int previous_tool;

	public Color unusable_color;
	public Color previous_color;
	private Material tool_color;
	public GameObject tool;
	public LineRenderer lineRenderer;

    public PlayerManagerScript playerManager;

	// Use this for initialization
	void Start () {
		Debug.Log ("Tool object activé");
		origin_pos = origin.GetComponent<Transform> ();
		unusable_color = Color.black;
        tool_color = tool.GetComponent<MeshRenderer>().material;
        playerManager = GameObject.Find("PlayerManager").GetComponent<PlayerManagerScript>();
	}
	
	// Update is called once per frame
	void Update () {
		if (!isLocalPlayer) {
			return;
		}
		testButtons ();
       // if(playerManager.CanPlayer3Act())
		Vector3 posOrigin = new Vector3();

		if (Input.GetMouseButtonDown (0))
			Debug.Log (toolState);
		
        switch (toolState) { 

			case AWAITING_RELOAD:
				if (!isWaiting)
				{
					previous_color = tool_color.color;
					tool_color.color = unusable_color;
					delay = current_tool_delay;
					isWaiting = true;
				}
				float percent = delay / current_tool_delay;
				tool_color.color = (1 - percent) * previous_color + (percent) * unusable_color;
			delay -= Time.deltaTime;
			if (delay < 0) {
				isWaiting = false;
				toolState = previous_tool;
			}
			break;

			case GUN_TOOL_SELECTED:
			posOrigin = origin.GetComponent<Transform>().position;
			displayLineRenderer(posOrigin);
            if (toolActivated()) {
                CmdFireBullet(destination.transform.position, posOrigin);
					current_tool_delay = GUN_TOOL_RELOAD_TIME;
					previous_tool = GUN_TOOL_SELECTED;
				toolState = AWAITING_RELOAD;
            }
            break;

		case HEAL_GUN_TOOL_SELECTED:
			posOrigin = origin.GetComponent<Transform>().position;
			displayLineRenderer(posOrigin);
			if (toolActivated()) {
					CmdFireHealBullet(destination.transform.position, posOrigin);
					previous_tool = HEAL_GUN_TOOL_SELECTED;
				toolState = AWAITING_RELOAD;
				current_tool_delay = HEAL_GUN_TOOL_RELOAD_TIME;
			}
			break;

		case KILL_GUN_TOOL_SELECTED:
			posOrigin = origin.GetComponent<Transform>().position;
			displayLineRenderer(posOrigin);

			if (toolActivated()) {
					CmdFireHealBullet(destination.transform.position, posOrigin);
					previous_tool = KILL_GUN_TOOL_SELECTED;
				toolState = AWAITING_RELOAD;
				current_tool_delay = KILL_GUN_TOOL_RELOAD_TIME;
			}
			break;

		case WALL_TOOL_SELECTED:
				posOrigin = origin.GetComponent<Transform>().position;
				displayLineRenderer(posOrigin);

				if (toolActivated()) {
					CmdWallSpawn(getPointedPosition());
					previous_tool = WALL_TOOL_SELECTED;
					toolState = AWAITING_RELOAD;
					current_tool_delay = WALL_RELOAD_TIME;
				}
			break;

			case COOP_TOOL_SELECTED:

				if (toolActivated ())
				{
					CmdCoopTool ();
					previous_tool = COOP_TOOL_SELECTED;
					toolState = AWAITING_RELOAD;
					current_tool_delay = COOP_RELOAD_TIME;
				}
				break;
        }

		toolSwitch ();

    }

	void testButtons()
	{
		if(Input.GetButtonDown("TriggerL"))
			Debug.Log("TriggerL activated");

		if(Input.GetButtonDown("TriggerR"))
			Debug.Log("TriggerR activated");
		
		if(Input.GetButtonDown("TouchL"))
			Debug.Log("TouchL activated");

		if(Input.GetButtonDown("TouchR"))
			Debug.Log("TouchR activated");
		
		if(Input.GetAxis("GripL") == 1.0f)
			Debug.Log("GripL activated");

		if(Input.GetAxis("GripR") == 1.0f)
			Debug.Log("GripR activated");
	}

    void toolSwitch()
    {
		if(toolSwitched()){
        switch (toolState)
        {
			
				case GUN_TOOL_SELECTED:
					tool_color.color = Color.green;
                    toolState = HEAL_GUN_TOOL_SELECTED;
                break;

            case HEAL_GUN_TOOL_SELECTED:
					tool_color.color = Color.red;
                toolState = KILL_GUN_TOOL_SELECTED;
                break;

            case KILL_GUN_TOOL_SELECTED:
					tool_color.color = Color.blue;
                toolState = WALL_TOOL_SELECTED;
                break;

            case WALL_TOOL_SELECTED:
					tool_color.color = Color.magenta;
                toolState = COOP_TOOL_SELECTED;
                break;


            case COOP_TOOL_SELECTED:
					tool_color.color = Color.white;
                toolState = GUN_TOOL_SELECTED;
                break;
			}
        }
    }

    bool toolActivated()
    {
       // if (UnityEngine.Input.GetJoystickNames().ToString().Contains("OpenVR"))
         //   return Input.GetButtonDown();

        return Input.GetMouseButtonDown(0);
	}

	bool toolSwitched(){

		/*
                 * if (Input.getButton())
                 *      toolState =
                 * if (Input.getButton())
                 *      toolState =
                 */
		return Input.GetMouseButtonDown(1);
	}

	void displayLineRenderer(Vector3 posOrigin) {
		if (Input.GetMouseButton (0)) {

			lineRenderer.enabled = true;
			Vector3 posDest = getPointedPosition ();
			lineRenderer.SetPosition (0, posOrigin);
			lineRenderer.SetPosition (1, posDest);
		} else
			lineRenderer.enabled = false;
	}

    Vector3 getPointedPosition()
    {
        RaycastHit hit;
		Ray ray = new Ray(destination.transform.position, destination.transform.position - origin.transform.position);
		if (Physics.Raycast (ray, out hit)) {
			return hit.point;
		} else
			return hit.point;
    }

    [Command]
    void CmdFireBullet(Vector3 dest, Vector3 origin)
    {
        GameObject obj = Instantiate(bullet_prefab, dest, Quaternion.identity) as GameObject;
        Vector3 direction = dest - origin;
        obj.transform.rotation = Quaternion.LookRotation(direction);
        obj.GetComponent<Rigidbody>().velocity = obj.GetComponent<Transform>().forward.normalized * PROJECTILE_SPEED;
        NetworkServer.Spawn(obj);
        Debug.Log("bullet spawned");
    }

    [Command]
    void CmdFireHealBullet(Vector3 dest, Vector3 origin)
    {
        GameObject obj = Instantiate(heal_bullet_prefab, dest, Quaternion.identity) as GameObject;
        Vector3 direction = dest - origin;
        obj.transform.rotation = Quaternion.LookRotation(direction);
        obj.GetComponent<Rigidbody>().velocity = obj.GetComponent<Transform>().forward.normalized * PROJECTILE_SPEED;
        NetworkServer.Spawn(obj);
        Debug.Log("heal bullet spawned");
    }


	[Command]
	void CmdFireKillBullet(Vector3 dest, Vector3 origin)
	{
		GameObject obj = Instantiate(kill_bullet_prefab, dest, Quaternion.identity) as GameObject;
		Vector3 direction = dest - origin;
		obj.transform.rotation = Quaternion.LookRotation(direction);
		obj.GetComponent<Rigidbody>().velocity = obj.GetComponent<Transform>().forward.normalized * PROJECTILE_SPEED;
		NetworkServer.Spawn(obj);
		Debug.Log("kill bullet spawned");
	}


    [Command]
    void CmdWallSpawn(Vector3 dest)
    {
		if (dest.y < 1.0e-14)
        {
            GameObject obj = Instantiate(wall_prefab, new Vector3(dest.x, -10, dest.z), Quaternion.identity);
			obj.GetComponent<Transform> ().localScale = new Vector3 (0.5f, 20.0f, 0.5f);
            NetworkServer.Spawn(obj);
            Debug.Log("wall spawned");
        }
        else
			Debug.Log("impossible de spawn un mur, sol non pointé : " + dest.y + "    "+ (10^-14) * 1.0f);
    }

	[Command]
	void CmdCoopTool()
	{
		GameObject playerManager = GameObject.Find ("PlayerManager");
		PlayerManagerScript playerManagerSc = playerManager.GetComponent<PlayerManagerScript> ();
		if (playerManagerSc == null)
		{
			Debug.Log("Erreur : Player Manage introuvable !");
			return;
		}
		playerManagerSc.ReceiveParalysisAttempt();
	}
}
