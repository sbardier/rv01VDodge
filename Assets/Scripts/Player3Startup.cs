﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;



public class Player3Startup : NetworkBehaviour {

    public GameObject baseObject;

	// Use this for initialization
	void Start () {
        if (!isLocalPlayer)
            return;
        baseObject.SetActive(true);
    }

    // Update is called once per frame
    void Update () {
		
	}
}
