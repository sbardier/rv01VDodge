﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class TransformLocalCopy : NetworkBehaviour {

	public float multPosition = 1.0f;
	public float multRotation = 1.0f;
    public Transform vrTransform;
    Transform myTransform;

	Vector3 backup;

    // Use this for initialization
    void Start ()
    {
        if (!isLocalPlayer)
            return;
		myTransform = GetComponent<Transform>();
		myTransform.localPosition = vrTransform.localPosition;
		myTransform.localRotation = vrTransform.localRotation;
		backup = vrTransform.localPosition;
    }

    // Update is called once per frame
    void Update() {

        if (!isLocalPlayer)
            return;
		myTransform.localPosition += multPosition * (vrTransform.localPosition - backup);
		backup = vrTransform.localPosition;
		//myTransform.localPosition = vrTransform.localPosition;
		myTransform.localRotation = vrTransform.localRotation;
		//Debug.Log ("Posi - backup = " + (multPosition * (vrTransform.localPosition - backup)).ToString());
	}
}
