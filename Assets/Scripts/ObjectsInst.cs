﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class ObjectsInst : NetworkBehaviour {
    Ray ray;
    RaycastHit hit;

    public GameObject cube;
    public GameObject sphere;
    public GameObject grill1, grill2, grill3;
    public GameObject turret;
    public GameObject turret2;
    public GameObject turret3;
    public GameObject wall;
    public Canvas canvasPlayer3;

    public float height;
    public Slider mainSlider;
    public Slider simpleSlider;

    public ToggleGroup toggleGroup;
    public ToggleGroup toggleGroupTurret;

    public GameObject modifyTurret;
    public GameObject modifySimple;

    // private string player1str = "Player(Clone)";
    //private string player2str = "Player2(Clone)";

    //public string message;//the message we want to write
    //public int wordLimit;//how many letters we alow in a message
    //public ChatManager Manager;//the chatmanager which shold be on the gamemanger

    GameObject center;

    private GameObject plane; // L'objet plan de la scène
    Bounds planeBounds; // Limites du plan


    private bool down = false;
    private float xA = 0, zA = 0;

    private int grillCounter = 1;

    private GameObject lastObject = null, actualObject = null;
    private Color lastColor = Color.red, auxColor = Color.red;

    private InputField inputSpeed, inputFireRate, inputPrecision;

    private PlayerManagerScript player3_can_act;

    // Use this for initialization
    void Start() {

        if (!isLocalPlayer && !isServer)
            return;

        if (isLocalPlayer)
        {
            canvasPlayer3.enabled = true;
            Debug.Log("Canvas Player 3 enabled");
        }
        else {
            Debug.Log("Canvas not enabled");


        }
            

        center = GameObject.Find("Center");
        plane = GameObject.Find("Plane");
        planeBounds = plane.GetComponent<Renderer>().bounds;

        StartGameObjects(1.0f, 0.8f, 0.3f);

        //Manager = GameObject.Find("NetworkManager").GetComponent<ChatManager>();
    }

    void StartGameObjects(float speed, float fireRate, float precision)
    {

        foreach (Transform child in modifyTurret.transform)
        {
            if (child.name == "Speed")
            {
                GameObject spee = child.gameObject;
                inputSpeed = spee.GetComponent<InputField>();
                inputSpeed.text = speed.ToString();
            }

            if (child.name == "FireRate")
            {
                GameObject firer = child.gameObject;
                inputFireRate = firer.GetComponent<InputField>();
                inputFireRate.text = fireRate.ToString();
            }

            if (child.name == "Precision")
            {
                GameObject preci = child.gameObject;
                inputPrecision = preci.GetComponent<InputField>();
                inputPrecision.text = precision.ToString();
            }
        }
    }

    // Update is called once per frame
    void Update() {

        if (!isLocalPlayer)
            return;
        //Debug.Log("trying too hard");

        ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            actualObject = hit.transform.gameObject;

            if (actualObject.tag == "Target")
            {
                SelectObj();
            }

            if (actualObject.tag == "Scenario")
            {
                instantiateObj();
            }

        }
    }


    Toggle GetActiveToggle(ToggleGroup group)
    {
        // get first active toggle (and actually there should be only one in a group)
        foreach (var item in group.ActiveToggles())
        {
            //Debug.Log("The active toggle in turret " + item.name);
            return item;
        }

        return null;
    }

    public void SelectObj()
    {
        if (Input.GetKey(KeyCode.Mouse0) && !down)
        {
            if (actualObject != lastObject)
            {
                auxColor = actualObject.GetComponent<Renderer>().material.color;
                Debug.Log(auxColor);

                if (lastObject != null)
                    ResetColor();

                if (actualObject.tag != "Scenario")
                {
                    actualObject.GetComponent<Renderer>().material.color = Color.yellow;

                    lastObject = actualObject;
                    lastColor = auxColor;

                    modifyManager();
                }
            }

            down = true;
        }

        if (!Input.GetKey(KeyCode.Mouse0) && down)
            down = false;
    }

    void modifyManager()
    {
        Debug.Log("Entró en modifyManager");
        Debug.Log(actualObject.name);

        if (actualObject.name == turret.name + "(Clone)" || actualObject.name == turret2.name + "(Clone)" || actualObject.name == turret3.name + "(Clone)")
        {
            FixTurretScript fts = actualObject.GetComponent<FixTurretScript>();
            StartGameObjects(fts.speed, fts.fireRate, fts.precision);

            modifyTurret.SetActive(true);
            modifySimple.SetActive(false);

            foreach (Transform child in modifyTurret.transform)
            {
                if (child.name == "Button")
                {
                    GameObject change = child.gameObject;
                    change.SetActive(true);
                }
            }
        }
        else if (actualObject.name == cube.name + "(Clone)" || actualObject.name == sphere.name + "(Clone)" || actualObject.name == wall.name + "(Clone)")
        {
            modifySimple.SetActive(true);
            modifyTurret.SetActive(false);
        }
    }

    public void ChangeTurret()
    {

        Toggle tt = GetActiveToggle(toggleGroupTurret);
        if (tt == null)
            return;


        var x = lastObject.GetComponent<FixTurretScript>();

        if (tt.name == "player1tt")
        {
            x.setPlayerTarget(1);
        }
        else if (tt.name == "player2tt")
        {
            x.setPlayerTarget(2);
        }


        float ispeed, ifirerate, iprecision;
        if (float.TryParse(inputSpeed.text, out ispeed) && float.TryParse(inputFireRate.text, out ifirerate) && float.TryParse(inputPrecision.text, out iprecision))
            x.setParameters(ispeed, ifirerate, iprecision);
        else Debug.Log("Turret's inputfield values are incorrect");

        modifyTurret.SetActive(false);
        ResetColor();
    }


    public void ChangeSimple()
    {
        Vector3 scale = lastObject.transform.localScale;

        scale.x = scale.x * simpleSlider.value * 2;
        scale.z = scale.z * simpleSlider.value * 2;
        if (lastObject.name != wall.name + "(Clone)")
            scale.y = scale.y * simpleSlider.value * 2;

        simpleSlider.value = 0.5f;

        Debug.Log(scale);
        CmdSpawn(lastObject, scale);

        ResetColor();

    }


    void ResetColor()
    {
        if (lastObject != null) {
            lastObject.GetComponent<Renderer>().material.color = lastColor;
            lastObject = null;
            actualObject = null;

            modifyTurret.SetActive(false);
            modifySimple.SetActive(false);
        }
    }

    public void ActivateTurretTools()
    {
        modifySimple.SetActive(false);


        Debug.Log("Entró a ActivateTurrentTools");
        Toggle t = GetActiveToggle(toggleGroup);
        Debug.Log(t.name);
        if (t != null)
        {
            if (t.name == "FixTurret" || t.name == "ToggleTurret2" || t.name == "ToggleTurret3")
            {
                if (t.name == "FixTurret")
                {
                    StartGameObjects(1.0f, 0.8f, 0.3f);
                }
                else if (t.name == "ToggleTurret2")
                {
                    StartGameObjects(3.0f, 3.0f, 0.2f);
                }
                else if (t.name == "ToggleTurret3")
                {
                    StartGameObjects(0.5f, 0.5f, 0.5f);
                }

                modifyTurret.SetActive(true);
                foreach (Transform child in modifyTurret.transform)
                {
                    if (child.name == "Button")
                    {
                        GameObject change = child.gameObject;
                        change.SetActive(false);
                    }
                }
            }
            else
            {
                modifyTurret.SetActive(false);
            }
        }
    }

    void instantiateObj()

    {
      //  if (player3_can_act.CanPlayer3Act()) {
        Toggle t = GetActiveToggle(toggleGroup);

        if (t == null && planeBounds.min.x < hit.point.x && hit.point.x < planeBounds.max.x
                    && planeBounds.min.z < hit.point.z && hit.point.z < planeBounds.max.z)
        {
            Debug.Log("There is no objet selected");
        }
        else if (t.name == wall.name)
        {
            if (Input.GetKey(KeyCode.Mouse0))
            {
                if (Mathf.Sqrt((xA - hit.point.x) * (xA - hit.point.x) + (zA - hit.point.z) * (zA - hit.point.z)) > .5)
                {
                    CmdCreateObject(wall.name, new Vector3(hit.point.x, -10, hit.point.z), Quaternion.identity);

                }
                xA = hit.point.x;
                zA = hit.point.z;
            }
        }
        else if (Input.GetKey(KeyCode.Mouse0) && !down)
        {
            if (t.name == "Cube")
            {
                CmdCreateObject(cube.name, new Vector3(hit.point.x, height, hit.point.z), Quaternion.identity);
            }
            else if (t.name == "Sphere")
            {
                CmdCreateObject(sphere.name, new Vector3(hit.point.x, height, hit.point.z), Quaternion.identity);
            }
            else if (t.name == "FixTurret")
            {
                CmdCreateObject(turret.name, new Vector3(hit.point.x, 2.0f, hit.point.z), Quaternion.identity);
            }
            else if (t.name == "ToggleTurret2")
            {
                CmdCreateObject(turret2.name, new Vector3(hit.point.x, 2.0f, hit.point.z), Quaternion.identity);
            }
            else if (t.name == "ToggleTurret3")
            {
                CmdCreateObject(turret3.name, new Vector3(hit.point.x, 2.0f, hit.point.z), Quaternion.identity);
            }
            else if (t.name == "Grill")
            {
                if (grillCounter > 3)
                    grillCounter = 1;

                if (grillCounter == 1)
                    CmdCreateObject(grill1.name, new Vector3(0, 15, planeBounds.max.z), Quaternion.identity);
                else if (grillCounter == 2)
                    CmdCreateObject(grill2.name, new Vector3(0, 15, planeBounds.max.z), Quaternion.identity);
                else if (grillCounter == 3)
                    CmdCreateObject(grill3.name, new Vector3(planeBounds.min.x + 5, 25, planeBounds.max.z), Quaternion.identity);

                grillCounter++;

            }


            down = true;

        }

        if (!Input.GetKey(KeyCode.Mouse0) && down)
            down = false;

        //}
    }

    /*
    [Command]
    public void CmdSendMessageToServer(string words)
    {
        RpcSentMessageToClients(words);
    }

    [ClientRpc]
    void RpcSentMessageToClients(string words)
    {
        //var arr = new Array ();
        //arr.push(words);
        Manager.setString(words as SyncListString);//arr.ToBuiltin(String) as String[];
    }*/

    [Command]
    public void CmdSpawn(GameObject obj, Vector3 scale) {
        Vector3 pos = obj.transform.position;
        string nameObj = obj.name;

        Destroy(obj);
        GameObject ob = null;

        if (nameObj == cube.name + "(Clone)")
        {
            ob = Instantiate(cube, pos, Quaternion.identity) as GameObject;
        }
        else if (nameObj == sphere.name + "(Clone)")
        {
            ob = Instantiate(sphere, pos, Quaternion.identity) as GameObject;
        }
        else if (nameObj == wall.name + "(Clone)")
        {
            ob = Instantiate(wall, pos, Quaternion.identity) as GameObject;
        }


        ob.transform.localScale = scale;
        
        NetCorrector nc = ob.GetComponent<NetCorrector>();
        nc.rotation = ob.transform.rotation;
        nc.scale = ob.transform.localScale;

        NetworkServer.Spawn(ob);

        var x = GetComponent<ChatSystem>();
        x.sendValues("Player 3 modified a " + ob.name + " ", pos);
    }

    [Command]
    public void CmdCreateObject(string prefabname, Vector3 position, Quaternion rotation)
    {
        if (lastObject != null)
            ResetColor();
        Debug.Log("nom du prefab : " + prefabname);

        GameObject obj = null;

        if (prefabname == cube.name)
            obj = Instantiate(cube, position, Quaternion.identity) as GameObject;
        if (prefabname == sphere.name)
            obj = Instantiate(sphere, position, Quaternion.identity) as GameObject;
        if (prefabname == wall.name)
            obj = Instantiate(wall, position, Quaternion.identity) as GameObject;

        if (prefabname == grill1.name)
            obj = Instantiate(grill1, position, Quaternion.identity) as GameObject;

        if (prefabname == grill2.name)
            obj = Instantiate(grill2, position, Quaternion.identity) as GameObject;

        if (prefabname == grill3.name)
            obj = Instantiate(grill3, position, Quaternion.identity) as GameObject;

        if (prefabname == turret.name || prefabname == turret2.name || prefabname == turret3.name)
        {
            if (prefabname == turret.name)
            {
                obj = Instantiate(turret, position, Quaternion.identity) as GameObject;

            }
            else if (prefabname == turret2.name)
            {
                obj = Instantiate(turret2, position, Quaternion.identity) as GameObject;
            }
            else if (prefabname == turret3.name)
            {
                obj = Instantiate(turret3, position, Quaternion.identity) as GameObject;
            }
            else {
                Debug.Log("It has occurred an error while creating the turret prefab");
                return;
            }

                Debug.Log(obj.name);
            FixTurretScript compo = obj.GetComponent<FixTurretScript>();
            if(compo != null) {
               

                Toggle tt = GetActiveToggle(toggleGroupTurret);//--
				if(tt != null){
                if (tt.name == "player1t")
                {
                    obj.GetComponent<FixTurretScript>().setPlayerTarget(1);
                }
                else if (tt.name == "player2t")
                {
                    obj.GetComponent<FixTurretScript>().setPlayerTarget(2);
                }//--
				}
                float ispeed, ifirerate, iprecision;
                if (float.TryParse(inputSpeed.text, out ispeed) && float.TryParse(inputFireRate.text, out ifirerate) && float.TryParse(inputPrecision.text, out iprecision))
                    obj.GetComponent<FixTurretScript>().setParameters(ispeed, ifirerate, iprecision);
                else Debug.Log("Turret's inputfield values are incorrect");

            }
            obj.transform.LookAt(center.transform.position);
        }

        if (prefabname != grill1.name && prefabname != grill2.name && prefabname != grill3.name && prefabname != turret.name && prefabname != turret2.name && prefabname != turret3.name)
        {

            /*
            
            */
            if(obj != null) { 
            //Debug.Log(prefab.name);
            Vector3 scale = obj.transform.localScale;
            scale.x = scale.z = mainSlider.value * 2;

            if (prefabname != "Wall")
                scale.y = mainSlider.value * 2;

            obj.transform.localScale = scale;
            }
        }
        
        NetCorrector nc = obj.GetComponent<NetCorrector>();
        if (nc != null)
        {
            nc.rotation = obj.transform.rotation;
            nc.scale = obj.transform.localScale;
        }

        if (obj == null)
            Debug.Log("Obj null pour ce prefab : " + prefabname);
        else
            NetworkServer.Spawn(obj);

        var x = GetComponent<ChatSystem>();
        x.sendValues("Player 3 created a " + prefabname + " ", position);

    }
}
