﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorielPlayerScript : MonoBehaviour {

    int state;
    bool doneReading;
    string texte;


    GameObject predBullet;
    GameObject turret;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        switch (state)
        {
            case 1:
                texte = "Bienvenue dans le monde de Virtual Dodge ! Vous pouvez ici tester votre adresse en réalité virtuelle via votre casque.";
                break;

            case 2:
                texte = "Dans ce jeu, vous devez éviter les projectiles qui arrivent dans votre direction. Pour cela, rien de plus simple, il suffit de bouger votre tête";

                break;

            case 3:
                GameObject.Instantiate(predBullet);
                texte = "Par exemple, voici un projectile arrivant dans votre direction. Vous pouvez apercevoir sa trajectoire via la ligne bleue qui en émane. Cette ligne devient jaune si jamais vous êtes sur la trajectoire du projectile. Vous perdez si vous êtes touchés 3 fois";
                break;

            case 4:
                texte = "Ici ce projectile est facile à éviter. Cependant, en situation de jeu, plusieurs projectiles peuvent arriver simultanément dans votre direction. à vous donc d'agir au mieux pour en éviter un maximum";
                break;

            case 5:
                GameObject.Instantiate(turret);
                texte = "En situation de jeu, les projectiles proviennent de tourelles. Elles tirent périodiquement des tourelles dans votre direction. En voici un exemple";
                break;

            case 6:
                texte = "Les tourelles ne sont cependant pas invincibles, et vous pouvez les détruire en utilisant votre télécommande";
                break;

            case 7:
                texte = "Quand celle-ci est blanche, vous pouvez appuyer sur la gâchette arrière de la manette pour tirer un projectile dans la direction pointée par votre télécommande. Si elle rentre en contact avec une tourelle, elle subira des dégâts";
                break;

            case 8:
                texte = "Tirez suffisamment de fois dans une tourelle pour la détruire ! Vous diminuerez ainsi le nombre de projectiles arrivant dans votre direction";
                break;

            case 9:
                texte = "Faites néanmoins attention, vous avez un léger délai entre chacune de vos balles.";
                break;

            case 10:
                texte = "Les balles ne sont pas les seuls outils à votre disposition. Vous pouvez changer d'outil en appuyant sur l'un des boutons sur le côté de votre télécommande";
                break;

            case 11:
                texte = "La couleur de votre télécommande indique l'outil que vous sélectionnez \n" +
                    "Blanc : Projectile simple qui peut détruire les tourelles \n" +
                    "Vert : Tire un lent projectile qui, s'il touche l'autre joueur, lui permet de regagner un point de vie + \n" +
                    "Rouge : Tire un lent projectile qui, s'il touche l'autre joueur, lui fait perdre un point de vie + \n" +
                    "Bleu : Crée un mur protecteur qui bloque les balles à l'endroit que vous pointez + \n" +
                    "Rose : Un outil qui doit être utilisé en même temps que l'autre joueur pour fonctionner. Il empêche le troisième joueur de rajouter des projectiles pendant un instant";
                break;

            case 12:
                texte = "Faites attention tout de même, car certains outils peuvent prendre beaucoup de temps à recharger et vous laisser sans défense !";
                break;

            case 13:
                texte = "Dans cette simulation, le joueur sur ordinateur se chargera de placer différents projectiles. à vous de survivre le plus longtemps possible ! + \n" +
                    "Vous pouvez décider de coopérer avec votre allié pour survivre plus longtemps, ou à l'inverse tenter de l'éliminer si le coeur vous en dit.";
                break;

            case 14:
                texte = "Astuce : éviter les balles est une technique valable. Mais il est également possible de les dévier avec votre télécommande sans contrepartie. à vous de juger la méthode que vous préférez.";
                break;

            case 15:
                texte = "Vous savez désormais tout pour jouer au jeu. Tentez de survivre le plus longtemps possible ou plus longtemps que votre partenaire, à vous de définir votre objectif !";
                break;

        }
	}
}
