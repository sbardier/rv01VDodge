﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine;

public class TimerManager : NetworkBehaviour
{


    
    public float timeLeft = 30f;

    private bool activeCountDown = false;
    public Text timerText;

    // Use this for initialization
    void Start()
    {
        activeCountDown = true;
    }



    // Update is called once per frame
    void Update()
    {
       if (activeCountDown)
        {
            timeLeft -= Time.deltaTime;
            timerText.text = "Time: " + timeLeft.ToString();
            if (timeLeft < 0)
            {
                GameOver();
                activeCountDown = false;
            }


        }
    }

    private void GameOver()
    {
        timerText.text = "Game Over: Time finished";
    }
}
