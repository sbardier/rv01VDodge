﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;


public class playerReader : NetworkBehaviour {

    public Camera cam;
    public Text objectInfo;
    public ChatManager Manager;
    public Canvas canvasPlayer;

    void Start() {
        Manager = GameObject.Find("NetworkManager").GetComponent<ChatManager>();//find the gameManager object and the chat Manager script

        
        if (!isLocalPlayer)
        {
            canvasPlayer.enabled = true;
            Debug.Log("Canvas player enabled");
        }
        else {
            Debug.Log("Canvas player not enabled");
        }
    }

    void Update() {
		if (Manager.mArray.Count > 0)
		{
			if (Manager.mArray [Manager.mArray.Count - 1] != null)
			{
            
				float xDif, zDif;
				xDif = Manager.pArray [Manager.mArray.Count - 1].x - transform.position.x;
				zDif = Manager.pArray [Manager.mArray.Count - 1].z - transform.position.z;

				float angle = (float)((System.Math.Atan2 (zDif, xDif) / System.Math.PI) * 180f);
				if (angle < 0)
					angle += 360f;

				double angleM = -angle + 90;
				if (angleM < 0)
					angleM = angleM + 360;

				float[] pos;
				int i;
				pos = new float[4];

				pos [0] = transform.eulerAngles.y - 45;
				if (pos [0] < 0)
					pos [0] += 360;


				for (i = 1; i < 4; i++)
				{
					pos [i] = pos [i - 1] + 90;
					if (pos [i] > 360)
						pos [i] -= 360;

				}

				int fpos = 9;
				for (i = 0; i < 4; i++)
				{
					if (i != 3)
					{
						if (pos [i] < angleM && angleM < pos [i + 1])
						{                        
							fpos = i;
							break;
						}
					}
					if (i == 3)
					{
						if (pos [i] < angleM && angleM < pos [0])
						{
							fpos = i;
							break;
						}
					}

				}

				if (fpos == 9)
				{
					float aux = pos [0];
					fpos = 0;
					for (i = 1; i < 4; i++)
					{
						if (aux < pos [i])
						{
							aux = pos [i];
							fpos = i;
						}
					}
				}

				string msg = "Not good";

				if (fpos == 0)
					msg = "in front of you!";
				else if (fpos == 1)
					msg = "at your right!";
				else if (fpos == 2)
					msg = "behind of you!";
				else if (fpos == 3)
					msg = "at your left!";

				objectInfo.text = Manager.mArray [Manager.mArray.Count - 1] + msg;
            
			}
		}
    }
}
