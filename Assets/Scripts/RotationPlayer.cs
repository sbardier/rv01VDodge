﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class RotationPlayer : NetworkBehaviour {

	// Use this for initialization
	void Start () {
        if (!isLocalPlayer) {
            return;
        }
	}
	
	// Update is called once per frame
	void Update () {
        var x = 0f;
        if (Input.GetKey(KeyCode.Y)) {

            x = -500f * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.U))
        {

            x = 500 * Time.deltaTime;
            
        }

        transform.Rotate(0, x, 0);



    }
}
