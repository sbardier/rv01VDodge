﻿using UnityEngine;
using UnityEngine.Networking;


public class TrackingPlayer : NetworkBehaviour
{
   
    public float shootForce;
    public float  MAXTIME;
    private float timeCounter;
    private Vector3 AB;

    public GameObject ball;
    public GameObject myLine;


    private string targetName = "";
    private GameObject target = null;

	void Start ()
    {     
        timeCounter = 0;
    }
	
	
    private void FixedUpdate()
    {
        if (target != null) CmdShoot();
        else { findAgain(); }
        

    }

    public void SetTarget(string a) {
        targetName = a;
        Debug.Log("SetTarget: SpawnPosition1/" + a);
        target = GameObject.Find("SpawnPosition1/" + a);
    }

    void findAgain() {
        if (targetName != "") {
            target = GameObject.Find("SpawnPosition1/" + targetName);
        }
    }

    [Command]
    void CmdDrawLine()
    {
        GameObject l;
        l = Instantiate(myLine, Vector3.zero, Quaternion.identity) as GameObject;
        LineRenderer lr = l.GetComponent<LineRenderer>();
        lr.startColor = Color.red;
        lr.endColor = Color.yellow;
        lr.startWidth = 0.1f;
        lr.endWidth = 0.1f;

        lr.SetPosition(0, transform.position);
        lr.SetPosition(1, target.transform.position);
        GameObject.Destroy(l, 0.2f);
        NetworkServer.Spawn(l);
    }

    [Command]
    void CmdShoot()
    {
        AB = target.transform.position - transform.position;
        Vector3 scal = new Vector3(0.5f, 0.5f, 0.5f);

        AB.Normalize();
        AB = Vector3.Scale(AB, scal);

        //DrawLine(transform.position + AB, target.transform.position, Color.red);
        CmdDrawLine();
        if (timeCounter > MAXTIME)//Wait MAXTIME secs to shoot
        {
            GameObject c;
            c = Instantiate(ball, transform.position + AB, Quaternion.identity) as GameObject;
            Rigidbody rb = c.GetComponent<Rigidbody>();
            rb.AddForce(AB * shootForce);

            NetworkServer.Spawn(c);
            timeCounter = 0;
        }
        timeCounter += Time.deltaTime;

            
    }

}
