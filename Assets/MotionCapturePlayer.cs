﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class MotionCapturePlayer : NetworkBehaviour {


	public Transform head;
	public Transform controller;
	// Use this for initialization
	void Start () {
		Debug.Log (UnityEngine.Input.GetJoystickNames ());

	}

	// Update is called once per frame
	void Update () {
		if (!isLocalPlayer)
			return;
		if (head != null)
		{
			head.localPosition = UnityEngine.VR.InputTracking.GetLocalPosition (UnityEngine.VR.VRNode.Head);
			head.localRotation = UnityEngine.VR.InputTracking.GetLocalRotation (UnityEngine.VR.VRNode.Head);
		}

		if (controller != null)
		{
			controller.localPosition = UnityEngine.VR.InputTracking.GetLocalPosition (UnityEngine.VR.VRNode.LeftHand);
			controller.localRotation = UnityEngine.VR.InputTracking.GetLocalRotation (UnityEngine.VR.VRNode.LeftHand);
		}
	}
}
