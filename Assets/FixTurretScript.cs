﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


public class FixTurretScript : NetworkBehaviour {

    public float precision = 0.1f;
    public float fireRate = 0.1f;
    public float speed = 1.0f;

    public GameObject pointer;
    public GameObject[] players;
    public GameObject target;
    public GameObject prefab;

    private float timer;

    private bool activated = true;
	// Use this for initialization
	void Start () {
		if (!isServer)
			return;
		checkCurrentPlayers ();
	}
	
	// Update is called once per frame
	void Update () {
        if (!isServer)
            return;
        checkCurrentPlayers();
        timer += Time.deltaTime;
        if (target == null)
        {
            if (Random.Range(0, 2) > 0)
                setPlayerTarget(2);
            else
                setPlayerTarget(1);
        }
		if (timer > fireRate) {
            if(activated)
            FireAtPlayer();
            timer = 0;
        }
	}

    public void setParameters(float sp, float fR, float pre)
    {
        speed = sp;
        fireRate = fR;
        precision = pre;
    }

    public void setPlayerTarget(int i)
    {
        try
        {
        if (i == 1)
            target = players[0];
        if (i == 2)
            target = players[1];
            //do some work
        }
        catch (System.Exception exception)
        {
            Debug.Log("Tableau mal foutu, indice = " + i + " \ntaille tableau = " + players.Length + "\n" + exception.Message);
        }

    }

    void checkCurrentPlayers()
    {
        if (players.GetLength(0) != 2)
        {
            players = GameObject.FindGameObjectsWithTag("PlayerBody");
        }
    }

    public void FireAtPlayer()
    {
        Vector3 targ;
        Vector3 offset = new Vector3(Random.Range(-1,1), Random.Range(-1, 1), Random.Range(-1, 1)) * precision;
        Vector3 dest = target.GetComponent<Transform>().position + offset;
        Vector3 origin = pointer.GetComponent<Transform>().position;
        targ = dest - origin;
        GameObject bullet = Instantiate(prefab, origin, Quaternion.LookRotation(targ));
        Rigidbody rbBullet = bullet.GetComponent<Rigidbody>();
        rbBullet.velocity = (targ.normalized * speed);
        NetworkServer.Spawn(bullet);
        Destroy(bullet, 25.0f);
    }
}
